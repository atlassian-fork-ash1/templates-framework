package com.atlassian.confluence.plugin.templates.export;

import com.atlassian.confluence.pages.templates.PageTemplate;

import java.util.List;

public interface TemplatePackage {
    /**
     * Return a collection of the available templates offered by this plugin.
     * @return A {@link java.util.List} of {@link com.atlassian.confluence.pages.templates.PageTemplate}s.
     * @throws TemplatePackageException If an exception occurs.
     */
    List<PageTemplate> getAvailableTemplates() throws TemplatePackageException;

    /**
     * Returns the name for this template package
     *
     * @return The name of this package.
     */
    String getPackageName();
}
